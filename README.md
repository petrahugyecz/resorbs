[![ResOrbs](http://img.youtube.com/vi/WvibaQiUWBA/0.jpg)]

<br><strong> ResOrbs </strong> are fully customizable resource orbs for your GUI in Unity Editor. You can use it to display the current health / mana / any resource of the player character. <br> <br> 

<strong>&#11088;  But that's not all ... &#11088;</strong><br> You can create and custom progress animation, such as a loading screen. Also it's responsive, scales with the screen size, and you can use it on any platform.<br> <br>


&#10071;<strong>  Doesn’t compatible with the new UI Builder Package &#10071; </strong><br> <br>


<strong> Package contains: </strong><br>
- 16 orb types (560x...x1100 pixel, transparent background)<br>
- 12 frame types (250x...x1048 pixel, transparent background)<br> 
- 4 background (210x...x520 pixel, transparent background)<br>
- 3 fill scroll types (which you can combine, 256x...x600 pixel) <br>
- 3 glitter types (which you can combine, 200x...x480 pixel, transparent background) <br>
- 1 demo scene and 1 prefab
<br> <br>

<strong>Unity Asset Store</strong><br>
https://assetstore.unity.com/packages/tools/gui/resorbs-183965

<strong>Youtube preview</strong><br>
http://www.youtube.com/watch?v=WvibaQiUWBA

If you have any questions, suggestions or feedback, please feel free to leave a review or contact me at <a href="mailto:petrahugyecz@gmail.com"> petrahugyecz@gmail.com </a> &#9996;